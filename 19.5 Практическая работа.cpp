﻿#include <iostream>
#include <string>
using namespace std;


class Animal
{
public:
	virtual void Voice() {}
};

class Dog : public Animal
{
public:
	void Voice() override
	{
		cout << "Woof!" << endl;
	}
};

class Cat : public Animal
{
public:
	void Voice() override
	{
		cout << "Meow!" << endl;
	}
};

class Cow : public Animal
{
public:
	void Voice() override
	{
		cout << "Mu!" << endl;
	}
};


int main()
{
	const int Size = 3;
	Animal* Animals[Size] = {new Dog,new Cat,new Cow};
	for (int i = 0; i < Size; i++)
	{
		Animals[i]->Voice();
	}
}